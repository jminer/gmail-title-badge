/*
 * Copyright 2018 Jordan Miner
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

"use strict";

// If I try creating an HTMLImageElement with the favicon URL and drawing that into a canvas, then I
// can't convert the canvas to a data URL because it is tainted (even if I have permission for that
// domain in manifest.json!).
// https://stackoverflow.com/questions/19894948/canvas-has-been-tainted-by-cross-origin-data-via-local-chrome-extension-url

if(!window.gmailBadge) {
    const init = async () => {
        const getImageDataUrl = (imageUrl) => {
            return new Promise((resolve, reject) => {
                chrome.runtime.sendMessage({type: "getImageDataUrl", url: imageUrl}, resolve);
            });
        };

        const iconLink = document.querySelector('link[rel*="icon"]');

        // If I stop at a breakpoint here, the debugger shows chrome.runtime as undefined, but
        // it is not, as shown by a console.log().
        console.log(chrome.runtime);
        const oldIconUrl = await getImageDataUrl(iconLink.href);

        let newIconUrl = null;
        let badgeShown = false;
        window.gmailBadge = {
            initialized: false,
            showBadge: () => {
                iconLink.href = newIconUrl;
                badgeShown = true;
            },
            hideBadge: () => {
                iconLink.href = oldIconUrl;
                badgeShown = false;
            },
            toggleBadge: () => {
                // `this` here is Window, even though I'm calling window.gmailBadge.toggleBadge(),
                // so that seems to be a Chrome bug.
                badgeShown ? window.gmailBadge.hideBadge() : window.gmailBadge.showBadge();
            },
        };

        const image = document.createElement("img");
        image.onload = () => {
            const canvas = document.createElement("canvas");
            canvas.width = 32;
            canvas.height = 32;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0, 32, 32);
            ctx.ellipse(6, 6, 6, 6, 0, 0, 6.28);
            ctx.fillStyle = "rgb(73, 134, 231)";
            ctx.fill();
            newIconUrl = canvas.toDataURL();
            window.gmailBadge.initialized = true;
        };
        image.src = oldIconUrl;
    };

    init();
}

if(window.gmailBadge != null && window.gmailBadge.initialized) {
    window.gmailBadge.showBadge();
}