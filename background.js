/*
 * Copyright 2018 Jordan Miner
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

"use strict";

browser.tabs.onUpdated.addListener(async (id, changeInfo, tab) => {
    //console.log(changeInfo);
    if(changeInfo.title == null)
        return;
    if(tab.url == null || !tab.url.startsWith("https://mail.google.com/"))
        return;
    if(tab.active) // comment out to test
       return;
    //console.log("**Running CS**");
    await browser.tabs.executeScript(id, {
        file: "/gmail-content-script.js",
    });
});

browser.tabs.onActivated.addListener(async (activeInfo) => {
    const tab = await browser.tabs.get(activeInfo.tabId);
    //console.log(tab);
    if(tab.url == null || !tab.url.startsWith("https://mail.google.com/"))
        return;

    //console.log("**Clearing badge**");
    await browser.tabs.executeScript(activeInfo.tabId, {
        code: `
        if(window.gmailBadge && window.gmailBadge.initialized) {
            window.gmailBadge.hideBadge();
        }`,
    });
});

const getImageDataUrl = async (imageUrl) => {
    return new Promise((resolve, reject) => {
        const image = document.createElement("img");
        image.onload = () => {
            const canvas = document.createElement("canvas");
            canvas.width = image.naturalWidth;
            canvas.height = image.naturalHeight;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0);
            resolve(canvas.toDataURL());
        };
        image.src = imageUrl;
    });
};

browser.runtime.onMessage.addListener(async (message) => {
    if(message.type == "getImageDataUrl") {
        return await getImageDataUrl(message.url);
    }
});

// Can't filter in Chrome, only Firefox.
// {
//     urls: "*://mail.google.com/*",
//     properties: ["title"],
// }
